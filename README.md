# Emacs-Doom-Icon

This is my design for an icon for Doom Emacs.

I was inspired by this little guy. Link to the artists deviantart [here](https://www.deviantart.com/ultravioletbat/art/Yay-Evil-111710573).

| Original | Made for Doom Emacs |
| :-: | :-: |
| ![cacochan](https://raw.githubusercontent.com/hlissner/doom-emacs/screenshots/cacochan.png) | <img src="https://gitlab.com/wildwestrom/emacs-doom-icon/-/raw/master/Icon/cacodemon.svg" width="128" alt="Computer Hope"> |
